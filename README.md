# Flowtr CMS

A CMS made with Typescript,+ Fastify + MongoDB and Svelte + TailwindCSS.

## Documentation

As this project is currently in a development stage,
the documentation is not being worked on just yet.
Stay tuned for the alpha releases of Flowtr CMS.
