import fp from "fastify-plugin";
import { nanoid } from "nanoid";
import { ICollection } from "../util.js";

export default fp(async (app) => {
  const cmsCollection = app.db.collection<ICollection>("collection");

  app.route({
    method: "GET",
    url: "/collections",
    handler: async (req, res) => {
      return res.send({
        collections: await cmsCollection.find({}).toArray(),
      });
    },
  });

  app.route<{
    Params: {
      id: string;
    };
  }>({
    method: "GET",
    url: "/collections/:id",
    handler: async (req, res) => {
      const model = await cmsCollection.findOne({
        id: req.params.id,
      });

      if (!model)
        return res.status(404).send({
          errors: [
            {
              message: "Collection not found",
              data: {
                id: req.params.id,
              },
            },
          ],
        });

      res.send({
        model,
      });
    },
  });

  app.route<{
    Body: {
      collection: ICollection;
    };
  }>({
    method: "POST",
    url: "/collections",
    handler: async (req, res) => {
      const b = req.body.collection;

      const result = await cmsCollection.insertOne({
        name: b.name,
        id: nanoid(36),
        fields: {
          ...b.fields,
        },
      });

      return res.status(201).send({
        model: result,
      });
    },
  });

  app.route<{
    Params: {
      id: string;
    };
    Body: Record<string, unknown>;
  }>({
    method: "POST",
    url: "/collections/:id/create",
    handler: async (req, res) => {
      const c = await cmsCollection.findOne({
        id: req.params.id,
      });

      if (!c)
        return res.status(404).send({
          errors: [
            {
              message: "Collection not found",
              data: {
                id: req.params.id,
              },
            },
          ],
        });

      const mCol = app.db.collection(c.name);
      const result = mCol.insertOne(req.body);
      return res.status(201).send({ data: result });
    },
  });
});
