import fp from "fastify-plugin";
import { MongoSteel } from "mongosteel";
import { MongoClient, Db } from "mongodb";

declare module "fastify" {
  interface FastifyInstance {
    db: Db;
  }
}

export default fp(async (_app) => {
  if (!process.env.DB_URI)
    throw new Error("DB_URI environment variable is not defined");
  // await MongoSteel.connect(process.env.DB_URI);
  const client = new MongoClient(process.env.DB_URI);
  await client.connect();
  const db = client.db("cms");
  _app.decorate("db", db);
});
