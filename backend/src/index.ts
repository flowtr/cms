import autoLoad from "fastify-autoload";
import { fileURLToPath } from "url";
import { dirname, join } from "path";
import fastify, { FastifyReply, FastifyRequest } from "fastify";
import dotenv from "dotenv";
import fastifyCors from "fastify-cors";
import middie from "middie";
import { Logger } from "tslog";

dotenv.config();

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const app = fastify();
const logger = new Logger({
  displayFilePath: "hidden",
  displayFunctionName: false,
});

await app.register(middie);
await app.use((req: FastifyRequest, res: FastifyReply, next: () => void) => {
  logger.debug(req.url, req.method, req.ip, JSON.stringify(req.body, null, 2));
  next();
});

await app.setErrorHandler(async (err, req, res) => {
  logger.prettyError(err);
});

await app.register(fastifyCors);

await app.register(autoLoad, {
  dir: join(__dirname, "plugins"),
});

await app.register(autoLoad, {
  dir: join(__dirname, "controllers"),
});

const port = parseInt(process.env.PORT ?? "8008");
await app.listen(port);
console.info(`Listening on port ${port}`);
