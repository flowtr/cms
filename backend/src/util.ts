import { model, Schema } from "mongosteel";
import { Document } from "mongodb";

export interface ICollection extends Document {
  id: string;
  name: string;
  fields: IField[];
}

export interface IField {
  name: string;
  type: "number" | "date" | "text";
}

const colSchema = new Schema<ICollection>({
  id: "string",
  name: "string",
  fields: "mixed",
});

export const Collection = model("collection", colSchema, {});

export const toMongoType = (fieldType: IField["type"]) => {
  switch (fieldType) {
    case "date":
      return "string";
    case "number":
      return "number";
    default:
      return "string";
  }
};

export const toMongo = (c: ICollection) => {
  const cSchema = new Schema<Record<string, string | number | Date>>(
    Object.fromEntries(
      Object.entries(c.fields).map((e) => [e[0], toMongoType(e[1].type)])
    )
  );

  const cModel = model(c.name, cSchema as never, {});

  return { mSchema: cSchema, mModel: cModel };
};
