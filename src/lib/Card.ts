import type { IconDefinition } from "@fortawesome/free-solid-svg-icons";

export interface ICard {
  title: string;
  description: string;
  icon?: IconDefinition;
}