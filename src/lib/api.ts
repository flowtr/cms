import axios from "axios";

export const api = axios.create({
  baseURL: import.meta.env.API_URI?.toString() ?? "http://localhost:8008",
});

export interface ICollection {
  id: string;
  name: string;
  fields: IField[];
}

export interface IField {
  name: string;
  type: "number" | "date" | "text";
}
