import "@fontsource/jetbrains-mono";
import "svelte";
import "./index.css";
import App from "./App.svelte";

const root = document.getElementById("app");
if (!root) throw new Error("Missing root element!");

const app = new App({
  target: root
});

export default app;
